import Vue from 'vue';
import VueRouter from 'vue-router';
import Hello from '../components/HelloWorld.vue';
import Task from '../components/Task.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/hello',
    name: 'Hello',
    component: Hello,
  },
  {
    path: '/tasks',
    name: 'Task',
    component: Task,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
